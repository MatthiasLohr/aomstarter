# Age of Mythology  Starter

## Credits

### Age of Mythology Icon

The Age of Mythology Icon used in this application was created by Alexielios.

  * https://www.deviantart.com/alexielios/art/Icon-Age-of-Mythology-558914061
  * https://www.facebook.com/alexielios


### launch4j

We're using the launch4j project (https://github.com/TheBoegl/gradle-launch4j) for providing a windows executable file.


### Age of Mythology configuration

Source of the configuration for Age of Mythology:

  * https://www.evolvehq.com/games/445/topics/problem-with-ip-address
