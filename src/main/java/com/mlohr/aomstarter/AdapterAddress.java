package com.mlohr.aomstarter;

import java.net.InetAddress;

public class AdapterAddress {

    private InetAddress inetAddress;

    private String name;

    public AdapterAddress(InetAddress inetAddress, String name) {
        this.inetAddress = inetAddress;
        this.name = name;
    }

    @Override
    public String toString() {
        return String.format("%s (%s)", name, inetAddress.getHostAddress());
    }

    public InetAddress getInetAddress() {
        return inetAddress;
    }
}
