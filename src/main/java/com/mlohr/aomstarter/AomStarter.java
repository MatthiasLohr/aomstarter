package com.mlohr.aomstarter;

import javax.swing.*;

public class AomStarter {

    private static AomStarterWindow aomStarterWindow;

    public static void main(String[] args) {
        // set application icon

        // set application look and feel
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }
        // open window
        SwingUtilities.invokeLater(() -> {
            aomStarterWindow = new AomStarterWindow();
            aomStarterWindow.setVisible(true);
        });
    }
}
