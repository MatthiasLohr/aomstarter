package com.mlohr.aomstarter;

import javax.swing.filechooser.FileFilter;
import java.io.File;

public class AomExeFilter extends FileFilter {

    @Override
    public boolean accept(File file) {
        if (file.isDirectory()) {
            return true;
        }
        if ("aom.exe".equals(file.getName().toLowerCase())) {
            return true;
        }
        return false;
    }

    @Override
    public String getDescription() {
        return "Age of Mythology Executable";
    }
}
