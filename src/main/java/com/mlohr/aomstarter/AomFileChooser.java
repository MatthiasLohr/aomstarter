package com.mlohr.aomstarter;

import javax.swing.*;

public class AomFileChooser extends JFileChooser {

    public AomFileChooser() {
        super("Search aom.exe");
        setFileSelectionMode(FILES_ONLY);
        addChoosableFileFilter(new AomExeFilter());
        setAcceptAllFileFilterUsed(false);
    }
}
